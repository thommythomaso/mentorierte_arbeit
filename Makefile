SHELL:=/bin/bash

.PHONY: all 
all: convert_img
	latexmk --verbose --pdf report.tex
	latexmk --verbose --pdf report.tex

.PHONY: fast
fast:
	latexmk --verbose --pdf report.tex

.PHONY: clean_all
clean_all: clean_img
	latexmk -C
	rm -f report.bbl
	rm -f report.pdf

.PHONY: clean
clean:
	latexmk -c
	rm -f report.bbl
	rm -f report.pdf

.PHONY: clean_img
clean_img:
	rm -f src/img/*.pdf


.PHONY: convert_img
convert_img:
	@for f in $(shell find ./src/img/ -name '*.svg'); do inkscape -z $${f} --export-pdf=$${f//svg/pdf}; done
